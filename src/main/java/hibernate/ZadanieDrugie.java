package hibernate;

import javax.persistence.EntityManager;
import javax.swing.text.html.parser.Entity;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class ZadanieDrugie {
    private static EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();

    public List<Employee> ownersOfPhoneBrand(PhoneBrand phoneBrand) {
        return entityManager.createQuery("from Employee e where e.phone.brand = :brand", Employee.class)
                .setParameter("brand", phoneBrand)
                .getResultList();
    }

    public void deleteEmployee() {
        entityManager.getTransaction().begin();
        entityManager.createQuery("delete from Employee e where e.jobs is empty")
                .executeUpdate();
        entityManager.getTransaction().commit();
    }

    public void blocker() {
        entityManager.getTransaction().begin();
        List<Job> blockerJobs = entityManager.createQuery("from Job j where j.type = :type", Job.class)
                .setParameter("type", Type.Blocker)
                .getResultList();
        for (Job j : blockerJobs) {
            j.setTitle("BLOCKER! " + j.getTitle());
            entityManager.merge(j);
        }
        // lub: "update Job j set j.title = concat('Blocker!', j.title) where j.type = :type"
        entityManager.getTransaction().commit();
    }

    public List<Job> loadJobsPage(int pageNr) {
        return entityManager.createQuery("from Job", Job.class)
                .setFirstResult(pageNr * 3)
                .setMaxResults(3)
                .getResultList();
    }

    public void updateMailMine() {
        entityManager.getTransaction().begin();
        List<Employee> employees = entityManager.createQuery("from Employee e", Employee.class)
                .getResultList();
        for (Employee e : employees) {
            String email = e.getEmail();
            if (!email.equals(e.getName() + '.' + e.getSurname() + "@nowaDomena")) {
                e.setEmail(e.getName().toLowerCase() + '.' + e.getSurname().toLowerCase() + "@nowaDomena");
            }
            entityManager.merge(e);
        }
        // lub: "update Job j set j.title = concat('Blocker!', j.title) where j.type = :type"
        entityManager.getTransaction().commit();
    }
    public void updateMail() {
        entityManager.getTransaction().begin();
        List<Employee> employees = entityManager.createQuery("from Employee e where e.email is not concat(e.name,'.',e.surname,'@nowaDomena')", Employee.class)
                .getResultList();
        for (Employee e : employees) {
                e.setEmail(e.getName().toLowerCase() + '.' + e.getSurname().toLowerCase() + "@nowaDomena");
            entityManager.merge(e);
        }
        // lub: "update Job j set j.title = concat('Blocker!', j.title) where j.type = :type"
        entityManager.getTransaction().commit();
    }
    public void updateMailOne() {
        entityManager.getTransaction().begin();
        entityManager.createQuery("update Employee e set e.email = lower(concat(e.name,'.',e.surname,'@nowaDomena')) where e.email is not concat(e.name,'.',e.surname,'@nowaDomena')")
                .executeUpdate();
        // lub: "update Job j set j.title = concat('Blocker!', j.title) where j.type = :type"
        entityManager.getTransaction().commit();
    }

    public void changeJobs() {
        entityManager.getTransaction().begin();
        List<Employee> employees = entityManager.createQuery("from Employee e where e.jobs is empty", Employee.class)
                .getResultList();
        if (!employees.isEmpty()) {
            for (Employee e : employees) {
                String email = e.getEmail();
                if (!email.equals(e.getName() + '.' + e.getSurname() + "@nowaDomena")) {
                    e.setEmail(e.getName().toLowerCase() + '.' + e.getSurname().toLowerCase() + "@nowaDomena");
                }
                entityManager.merge(e);
            }
        }
        // lub: "update Job j set j.title = concat('Blocker!', j.title) where j.type = :type"
        entityManager.getTransaction().commit();
    }
}

