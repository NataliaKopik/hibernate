package hibernate;

public enum Type {
    Blocker,
    HighPriority,
    Normal
}
