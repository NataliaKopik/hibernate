package hibernate;

import javax.persistence.*;
import java.util.List;
@Entity
@Table
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;
    private String title;
    @ManyToMany
    @JoinTable(
            name="Project_Employee",
            joinColumns = {
                    @JoinColumn(name="project_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "employee_id")}
    )
    private List<Employee> employees;

    public Project(){};

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", employees=" + employees +
                '}';
    }
}
