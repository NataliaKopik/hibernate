package hibernate;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
// w hibernate.cfg.xml: <mapping class="hibernate.Employee"/> wychodzi na to, że musi być w jakimś package, bo inaczej nie działa

@Entity
@Table(name="Employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;
    @Column(name="name")
    private String name;
    @Column(name="surname")
    private String surname;
    @Column(name="date_of_birth")
    private LocalDate dateOfBirth;
    @Column(name="email")
    private  String email;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="phone_id")
    private Phone phone;
    @OneToMany (mappedBy = "employee")
    private List<Job> jobs;
    @ManyToMany(mappedBy = "employees")
    private List<Project> projects;

    public Employee(){};

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

