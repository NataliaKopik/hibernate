package hibernate;

import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class main {
    public static void main(String[] args) {
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        final EntityManager entityManager = sessionFactory.createEntityManager();

        Phone phone1 = new Phone();
        phone1.setBrand(PhoneBrand.Y);
        phone1.setModel("Y");
        Phone phone2 = new Phone();
        phone2.setBrand(PhoneBrand.X);
        phone2.setModel("B");

        Employee employee = new Employee();
        employee.setDateOfBirth(LocalDateTime.now().toLocalDate());
        employee.setEmail("jdoe@gmail.com");
        employee.setName("John");
        employee.setSurname("Doe");
        employee.setPhone(phone2);
        Employee employee2 = new Employee();
        employee2.setDateOfBirth(LocalDateTime.now().toLocalDate());
        employee2.setEmail("adoe@gmail.com");
        employee2.setName("Amy");
        employee2.setSurname("Doe");
        employee2.setPhone(phone1);
        List<Employee> employees = new LinkedList<>();
        employees.add(employee);
        employees.add(employee2);

        Job job1 = new Job();
        job1.setTitle("First job");
        job1.setDescription("Something about j1");
        job1.setEmployee(employee);
        job1.setType(Type.Blocker);
        Job job2 = new Job();
        job2.setTitle("Second job");
        job2.setDescription("Something about j2");
        job2.setEmployee(employee);
        job2.setType(Type.HighPriority);
        Job job3 = new Job();
        job3.setTitle("Third job");
        job3.setDescription("Something about j3");
        Job job4 = new Job();
        job4.setTitle("Fourth job");
        job4.setDescription("Something about j4");

        Project project1 = new Project();
        project1.setTitle("Project 1");
        Project project2 = new Project();
        project2.setTitle("Project 2");
        List<Project> projects = new LinkedList<>();
        projects.add(project1);
        projects.add(project2);
        project1.setEmployees(employees);

       // phone.setEmployee(employee); wystarczy przypisać obiekt1 do tego obiektu2, który ma właścicielstwo relacji, czyli kolumnę z FK
        entityManager.getTransaction().begin();
        entityManager.persist(employee);
        entityManager.persist(employee2);
        entityManager.persist(phone1);
        entityManager.persist(phone2);
        entityManager.persist(job1);
        entityManager.persist(job2);
        entityManager.persist(project1);
        entityManager.persist(project2);
        entityManager.persist(job3);
        entityManager.persist(job4);
        entityManager.getTransaction().commit();

        ZadanieDrugie zadanieDrugie = new ZadanieDrugie();
        List<Employee> results = zadanieDrugie.ownersOfPhoneBrand(PhoneBrand.Y);
        System.out.println(results);
        //zadanieDrugie.deleteEmployee();
        zadanieDrugie.blocker();

        List <Job> results2 = zadanieDrugie.loadJobsPage(0);
        System.out.println(results2);
        zadanieDrugie.updateMailOne();

        /* Day 1
        Session session = HibernateUtil.getSessionFactory().openSession();

        String sql = "select version()";

        String result = (String) session.createNativeQuery(sql).getSingleResult();
        System.out.println(result);

        session.close();

        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        final EntityManager entityManager = sessionFactory.createEntityManager();

        Employee employee = new Employee();
        employee.setDateOfBirth(LocalDateTime.now().toLocalDate());
        employee.setEmail("jdoe@gmail.com");
        employee.setName("John");
        employee.setSurname("Doe");
        Employee employee2 = new Employee();
        employee2.setDateOfBirth(LocalDate.of(1992, 10, 1));
        employee2.setEmail("adoe@gmail.com");
        employee2.setName("Amy");
        employee2.setSurname("Doe");
        entityManager.getTransaction().begin();
        entityManager.persist(employee);
        entityManager.persist(employee2);
        // entityManager.remove(employee);

        Employee employee3 = entityManager.find(Employee.class, 2);
        employee3.setName("zmiana");
        System.out.println(employee3);
        entityManager.merge(employee3);
        entityManager.getTransaction().commit();
        Query query = entityManager.createQuery("from Employee where surname= :surname");
        query.setParameter("surname", "Doe");
        List<Employee> employees = query.getResultList();
        System.out.println("Here:");
        System.out.println(employees);
        HibernateUtil.shutdown();
         */
    }
}
