package hibernate;


import javax.persistence.*;

@Entity
@Table
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;
    @Enumerated(EnumType.STRING)
    private PhoneBrand brand;
    private String model;

    @OneToOne(mappedBy = "phone",
            fetch = FetchType.EAGER)
    private Employee employee;

    Phone(){};

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PhoneBrand getBrand() {
        return brand;
    }

    public void setBrand(PhoneBrand brand) {
        this.brand = brand;
    }


}
